### Récupérération des dernières modifications du remote

```
git pull
cd backend
composer install
```

la commande `composer install` va installer toutes le dépendances présentes dans le fichier `composer.json`

### Créer la BDD
- Lancer le serveur mysql (xampp, wamp, mamp, ...)
- Dupliquer le fichier .env à la ravine du dossier 'backend' et le renommer en .env.local
- Commenter la ligne `DATABASE_URL="postgresql://app:!ChangeMe!@127.0.0.1:5432/app?serverVersion=15&charset=utf8"` avec un '#'
- Décommenter la ligne `# DATABASE_URL="mysql://<user-bdd>:!<mdp-bdd>!@127.0.0.1:3306/<nom-bdd>?serverVersion=10.11.2-MariaDB&charset=utf8mb4"`
	- Modifier `<user-bdd>` : nom d'utilisateur pour se connecter à la BDD
	- Modifier `<mdp-bdd>` : mdp pour se connecter à la BDD
	- Modifier `<nom-bdd>` : nom de la BDD -> agilitheque_db
	- Retirer la partie `<?serverVersion=10.11.2-MariaDB&charset=utf8mb4>`.

Lancer la suite de commande suivante :

```
cd backend
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

- 'php bin/console doctrine:database:create' : permet de créer la BDD sur le serveur mysql renseigné
- 'php bin/console doctrine:migrations:migrate' : permet de créer les tables
