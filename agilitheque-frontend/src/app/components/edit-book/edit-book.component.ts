import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Book, CreateBookDto } from '../../model/book.dto';
import { ImageInputComponent } from '../image-input/image-input.component';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { AuthorInputComponent } from '../author-input/author-input.component';
import { Author } from '../../model/author.dto';
import {MatSelectModule} from '@angular/material/select';
import { EmplacementDto } from '../../model/emplacement.dto';
import { EmplacementApiService } from '../../api/emplacement-api.service';
import { GenreDto } from '../../model/genre.dto';
import { EditionDto } from '../../model/edition.dto';
import { GenreApiService } from '../../api/genre-api.service';
import { EditionApiService } from '../../api/edition-api.service';

@Component({
  selector: 'app-edit-book',
  standalone: true,
  imports: [ImageInputComponent, FormsModule, ReactiveFormsModule, MatFormField, MatFormFieldModule, MatInputModule, MatButtonModule, MatSelectModule],
  templateUrl: './edit-book.component.html',
  styleUrl: './edit-book.component.scss'
})
export class EditBookComponent {
  @Input() book: Book | undefined = undefined;
  @Output() save: EventEmitter<CreateBookDto> = new EventEmitter<CreateBookDto>();

  formGroup: FormGroup = new FormGroup({
    title: new FormControl('', Validators.required),
    summary: new FormControl(''),
    annee: new FormControl('', Validators.required),
    emplacement: new FormControl('', Validators.required),
    genre: new FormControl('', Validators.required),
    edition: new FormControl('', Validators.required)
  });

  imgSrc? = '';
  selectedAuthor: Author | undefined = undefined;

  emplacements: EmplacementDto[] = [];
  genres: GenreDto[] = [];
  editions: EditionDto[] = [];

  constructor(public dialog: MatDialog, 
    private readonly emplacementApiService: EmplacementApiService, 
    private readonly genreApiService: GenreApiService,
    private readonly editionApiService: EditionApiService
  ) {}

  ngOnInit(): void {
    this.emplacementApiService.get().subscribe((emplacements: EmplacementDto[]) => {
      this.emplacements = emplacements;
    });
    this.genreApiService.get().subscribe((genres: GenreDto[]) => {
      this.genres = genres;
    });
    this.editionApiService.get().subscribe((editions: EditionDto[]) => {
      this.editions = editions;
    });
  }

  ngOnChanges(): void {
    if(this.book) {
      this.imgSrc = this.book.imageSrc;
      this.formGroup.controls['title'].setValue(this.book.libelle);
      this.formGroup.controls['summary'].setValue(this.book.resume);
      this.formGroup.controls['annee'].setValue(this.book.annee);
      this.formGroup.controls['emplacement'].setValue(this.book.emplacement?.id);
      this.formGroup.controls['genre'].setValue(this.book.genre?.id);
      this.formGroup.controls['edition'].setValue(this.book.edition?.id);
      this.selectedAuthor = this.book.auteur;
    }
    else {
      this.imgSrc = '';
      this.formGroup.controls['title'].setValue('');
      this.formGroup.controls['summary'].setValue('');
      this.formGroup.controls['annee'].setValue(2024);
      this.formGroup.controls['emplacement'].setValue(undefined);
      this.formGroup.controls['genre'].setValue(undefined);
      this.formGroup.controls['edition'].setValue(undefined);
      this.selectedAuthor = undefined;
    }
  }

  saveBook(): void {
    if(this.formGroup.valid && this.selectedAuthor?.id) {
      this.save.emit(
        {
          id: this.book?.id, 
          libelle: this.formGroup.controls['title'].getRawValue(),
          annee: this.formGroup.controls['annee'].getRawValue(),
          resume: this.formGroup.controls['summary'].getRawValue(), 
          imageSrc: this.imgSrc, 
          idAuteur: this.selectedAuthor.id,
          idEmplacement: this.formGroup.controls['emplacement'].getRawValue(),
          idGenre: this.formGroup.controls['genre'].getRawValue(),
          idEdition: this.formGroup.controls['edition'].getRawValue()
        });
    }
  }

  selectAuthor(): void {
    this.dialog.open(AuthorInputComponent, {
      height: '400px',
      width: '600px',
    }).afterClosed().subscribe((author: Author) => {
      this.selectedAuthor = author;
    });
  }
}
