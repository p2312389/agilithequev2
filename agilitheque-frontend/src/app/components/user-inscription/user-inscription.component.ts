import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, ReactiveFormsModule, FormsModule} from '@angular/forms';
import { UserService } from '../../services/user.service';
import {ImageInputComponent} from "../image-input/image-input.component";
import {MatFormField, MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatSelectModule} from "@angular/material/select";
import { Utilisateur } from '../../model/utilisateur.dto';

@Component({
  selector: 'app-user-inscription',
  standalone: true,
  templateUrl: './user-inscription.component.html',
  imports: [ImageInputComponent,
    FormsModule,
    ReactiveFormsModule,
    MatFormField,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule],
  styleUrls: ['./user-inscription.component.scss']
})
export class UserInscriptionComponent implements OnInit {
  inscriptionForm = new FormGroup({
    prenom: new FormControl('', Validators.required),
    nom: new FormControl('', Validators.required),
    login: new FormControl('', Validators.required),
    mot_de_passe: new FormControl('', Validators.required),
    role: new FormControl('user', Validators.required),
  });
  user : Utilisateur | null | undefined;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    if(this.user !== undefined && this.user !== null){
      this.inscriptionForm.controls['prenom'].setValue(this.user.prenom);
      this.inscriptionForm.controls['nom'].setValue(this.user.nom);
      this.inscriptionForm.controls['login'].setValue(this.user.login);
      this.inscriptionForm.controls['mot_de_passe'].setValue(this.user.password ?? null);
    }

  }

  onSubmit(): void {
    if(this.user !== undefined && this.user !== null){
      this.inscriptionForm.controls['prenom'].setValue(this.user.prenom);
      this.inscriptionForm.controls['nom'].setValue(this.user.nom);
      this.inscriptionForm.controls['login'].setValue(this.user.login);
      this.inscriptionForm.controls['mot_de_passe'].setValue(this.user.password ?? null);
    }
    if (this.inscriptionForm.valid) {
      this.user = {
        prenom: this.inscriptionForm.controls['prenom'].value ?? '',
        nom: this.inscriptionForm.controls['nom'].value ?? '',
        login: this.inscriptionForm.controls['login'].value ?? '',
        password: this.inscriptionForm.controls['mot_de_passe'].value ?? '',
        flag_niv: (this.inscriptionForm.controls['role'].value as "user" | "admin") ?? 'user'
      }
      //this.userService.createUser(this.user);
    }
    else {
      alert('Veuillez remplir tous les champs');
    }
  }
}
