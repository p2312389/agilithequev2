import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Reservation } from '../../model/reservation.dto';
import { DatePipe, UpperCasePipe } from '@angular/common';

@Component({
  selector: 'app-reservation-list',
  standalone: true,
  imports: [UpperCasePipe, DatePipe],
  templateUrl: './reservation-list.component.html',
  styleUrl: './reservation-list.component.scss'
})
export class ReservationListComponent {
  @Input() reservations: Reservation[] = [];
  @Input() selectedReservation: Reservation | undefined = undefined;
  @Output() selectedReservationChange: EventEmitter<Reservation> = new EventEmitter<Reservation>();

  select(reservation: Reservation): void {
    this.selectedReservationChange.emit(reservation);
  }
}
