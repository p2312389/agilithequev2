import { Component, EventEmitter, Input, Output } from '@angular/core';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { CreateUtilisateurDto, Utilisateur } from '../../model/utilisateur.dto';
import { MatSelectModule } from '@angular/material/select';

@Component({
  selector: 'app-edit-user',
  standalone: true,
  imports: [FormsModule, MatFormFieldModule, MatInputModule, MatButtonModule, ReactiveFormsModule, MatSelectModule],
  templateUrl: './edit-user.component.html',
  styleUrl: './edit-user.component.scss'
})
export class EditUserComponent {
  @Input() user: Utilisateur | undefined = undefined;
  @Output() save: EventEmitter<CreateUtilisateurDto> = new EventEmitter<CreateUtilisateurDto>();

  modifForm = new FormGroup({
    prenom: new FormControl('', Validators.required),
    nom: new FormControl('', Validators.required),
    login: new FormControl('', Validators.required),
    mot_de_passe: new FormControl(),
    role: new FormControl<'user' | 'admin'>('user', Validators.required),
  });
  
  ngOnChanges(): void {
    if(this.user) {
      this.modifForm.controls['prenom'].setValue(this.user.prenom);
      this.modifForm.controls['nom'].setValue(this.user.nom);
      this.modifForm.controls['login'].setValue(this.user.login);
      this.modifForm.controls['mot_de_passe'].setValue(this.user.password ?? null);
      this.modifForm.controls['role'].setValue(this.user.flag_niv);
    }
    else {
      this.modifForm.controls['prenom'].setValue('');
      this.modifForm.controls['nom'].setValue('');
      this.modifForm.controls['login'].setValue('');
      this.modifForm.controls['mot_de_passe'].setValue('');
      this.modifForm.controls['role'].setValue('user');
    }
  }


  saveUser(): void {
    if(this.modifForm.valid) {
      this.save.emit({
        id: this.user?.id,
        prenom: this.modifForm.controls['prenom'].value!,
        nom: this.modifForm.controls['nom'].value!,
        login: this.modifForm.controls['login'].value!,
        password: this.modifForm.controls['mot_de_passe'].value ?? undefined,
        flag_niv: this.modifForm.controls['role'].value!
      });
    }
  }

}
