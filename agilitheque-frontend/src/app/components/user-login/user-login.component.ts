import { Component } from '@angular/core';
import {UserService} from "../../services/user.service";
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {CommonModule} from "@angular/common";
import {MatFormField,MatFormFieldModule} from "@angular/material/form-field";
import {MatLabel} from "@angular/material/form-field";
import {ImageInputComponent} from "../image-input/image-input.component";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatSelectModule} from "@angular/material/select";

@Component({
  selector: 'app-user-login',
  standalone: true,
  imports: [ImageInputComponent,
    FormsModule,
    ReactiveFormsModule,
    MatFormField,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule],
  templateUrl: './user-login.component.html',
  styleUrl: './user-login.component.scss'
})
export class UserLoginComponent {
  connexionForm = new FormGroup({
    login: new FormControl('', Validators.required),
    mot_de_passe: new FormControl('', Validators.required),
  });

  errorMsg = '';

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.errorMsg = '';
    if (this.connexionForm.valid) {
      const login = this.connexionForm.controls['login'].value;
      const password = this.connexionForm.controls['mot_de_passe'].value;

      this.userService.login(login!, password!).subscribe({
        next: (user) => {
          this.router.navigate(['/home']);
        }, error: (error) => {
          this.errorMsg = 'Identifiants ou mots de passe incorrects';
        }
      });
    } else {
      this.errorMsg = 'Veuillez remplir tous les champs';
    }
  }
}
