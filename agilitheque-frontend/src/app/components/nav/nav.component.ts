import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { Router, RouterModule } from '@angular/router';
import {NgIf} from "@angular/common";
import {UserService} from "../../services/user.service";
import { Utilisateur } from '../../model/utilisateur.dto';

@Component({
  selector: 'app-nav',
  standalone: true,
  imports: [RouterModule, MatButtonModule, NgIf],
  templateUrl: './nav.component.html',
  styleUrl: './nav.component.scss'
})
export class NavComponent {
  user: Utilisateur | null | undefined;
  constructor(private userService: UserService, private router: Router) {
  }
  ngOnInit(): void {
    this.userService.userObservable.subscribe(user => this.user = user);
  }

  logout(): void {
    this.userService.logout();
    this.router.navigate(['/home']);
  }
}
