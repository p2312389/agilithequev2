import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Utilisateur } from '../../model/utilisateur.dto';

@Component({
  selector: 'app-user-list',
  standalone: true,
  imports: [],
  templateUrl: './user-list.component.html',
  styleUrl: './user-list.component.scss'
})
export class UserListComponent {
  @Input() users: Utilisateur[] = [];
  @Input() selectedUser: Utilisateur | undefined = undefined;
  @Output() selectedUserChange: EventEmitter<Utilisateur> = new EventEmitter<Utilisateur>();

  select(user: Utilisateur): void {
    this.selectedUserChange.emit(user);
  }

}
