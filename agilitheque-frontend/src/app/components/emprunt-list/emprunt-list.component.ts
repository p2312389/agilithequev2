import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Emprunt } from '../../model/emprunt.dto';
import { DatePipe, UpperCasePipe } from '@angular/common';

@Component({
  selector: 'app-emprunt-list',
  standalone: true,
  imports: [UpperCasePipe, DatePipe],
  templateUrl: './emprunt-list.component.html',
  styleUrl: './emprunt-list.component.scss'
})
export class EmpruntListComponent {
  @Input() emprunts: Emprunt[] = [];
  @Input() selectedEmprunt: Emprunt | undefined = undefined;
  @Output() selectedEmpruntChange: EventEmitter<Emprunt> = new EventEmitter<Emprunt>();

  select(emprunt: Emprunt): void {
    this.selectedEmpruntChange.emit(emprunt);
  }
}
