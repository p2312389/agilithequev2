import { HttpClient } from "@angular/common/http";
import { Observable, map } from "rxjs";
import { AuthorDto, CreateAuthorDto } from "../model/author.dto";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class AuthorApiService {
  constructor(private http: HttpClient) {}

  get(): Observable<AuthorDto[]> {
    return this.http.get<AuthorDto[]>(`${environment.API_URL}/auteurs`).pipe(map((response: any) => response['hydra:member']));
  }

  create(author: CreateAuthorDto): Observable<AuthorDto> {
    return this.http.post<AuthorDto>(`${environment.API_URL}/auteurs`, author).pipe(map((response: any) => response['hydra:member']));
  }

  update(author: CreateAuthorDto): Observable<AuthorDto> {
    return this.http.put<AuthorDto>(`${environment.API_URL}/auteurs`, author).pipe(map((response: any) => response['hydra:member']));
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${environment.API_URL}/auteurs/${id}`);
  }
}