import { HttpClient } from "@angular/common/http";
import { Observable, map } from "rxjs";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { EmplacementDto } from "../model/emplacement.dto";

@Injectable({
  providedIn: 'root'
})
export class EmplacementApiService {
  constructor(private http: HttpClient) {}

  get(): Observable<EmplacementDto[]> {
    return this.http.get<EmplacementDto[]>(`${environment.API_URL}/emplacements`).pipe(map((response: any) => response['hydra:member']));
  }
}