import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Book, BookDto, CreateBookDto } from '../model/book.dto';
import { environment } from '../../environments/environment';
import { CreateEmpruntDto, Emprunt, EmpruntDto } from '../model/emprunt.dto';

@Injectable({
  providedIn: 'root'
})
export class EmpruntApiService {
  constructor(private http: HttpClient) {}

  get(): Observable<Emprunt[]> {
    return this.http.get<EmpruntDto[]>(`${environment.API_URL}/emprunt`).pipe(map(emprunts => emprunts.map((emprunt: EmpruntDto) => new Emprunt(emprunt))));
  }

  create(emprunt: CreateEmpruntDto): Observable<Emprunt> {
    return this.http.post<EmpruntDto>(`${environment.API_URL}/emprunt`, emprunt).pipe(map(emprunt => new Emprunt(emprunt)));
  }

  update(emprunt: CreateEmpruntDto): Observable<any> {
    return this.http.put<EmpruntDto>(`${environment.API_URL}/emprunt/${emprunt.id}`, emprunt);
  }
}
