import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from '../../environments/environment';
import { CreateReservationDto, Reservation, ReservationDto } from '../model/reservation.dto';

@Injectable({
  providedIn: 'root'
})
export class ReservationApiService {
  constructor(private http: HttpClient) {}

  get(): Observable<Reservation[]> {
    return this.http.get<ReservationDto[]>(`${environment.API_URL}/reservation`).pipe(map(emprunts => emprunts.map((emprunt: ReservationDto) => new Reservation(emprunt))));
  }

  create(reservation: CreateReservationDto): Observable<Reservation> {
    return this.http.post<ReservationDto>(`${environment.API_URL}/reservation`, reservation).pipe(map(reservation => new Reservation(reservation)));
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${environment.API_URL}/reservation/${id}`);
  }
}
