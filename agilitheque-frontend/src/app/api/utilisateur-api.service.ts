import { HttpClient } from "@angular/common/http";
import { Observable, map } from "rxjs";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { CreateUtilisateurDto, Utilisateur, UtilisateurDto } from "../model/utilisateur.dto";

@Injectable({
  providedIn: 'root'
})
export class UtilisateurApiService {
  constructor(private http: HttpClient) {}

  get(): Observable<Utilisateur[]> {
    return this.http.get<UtilisateurDto[]>(`${environment.API_URL}/utilisateur`)
      .pipe(map(utilisateurs => utilisateurs.map(utilisateur => new Utilisateur(utilisateur))));
  }

  login(login: string, password: string): Observable<Utilisateur> {
    return this.http.post<UtilisateurDto>(`${environment.API_URL}/utilisateur/connexion`, { login, password })
      .pipe(map(utilisateur => new Utilisateur(utilisateur)));
  }

  create(utilisateur: CreateUtilisateurDto): Observable<Utilisateur> {
    return this.http.post<UtilisateurDto>(`${environment.API_URL}/utilisateur`, utilisateur)
      .pipe(map(utilisateur => new Utilisateur(utilisateur)));
  }

  update(utilisateur: CreateUtilisateurDto): Observable<Utilisateur> {
    return this.http.put<UtilisateurDto>(`${environment.API_URL}/utilisateur/${utilisateur.id}`, utilisateur)
      .pipe(map(utilisateur => new Utilisateur(utilisateur)));
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${environment.API_URL}/utilisateur/${id}`);
  }
}