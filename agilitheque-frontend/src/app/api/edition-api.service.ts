import { HttpClient } from "@angular/common/http";
import { Observable, map } from "rxjs";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { EditionDto } from "../model/edition.dto";

@Injectable({
  providedIn: 'root'
})
export class EditionApiService {
  constructor(private http: HttpClient) {}

  get(): Observable<EditionDto[]> {
    return this.http.get<EditionDto[]>(`${environment.API_URL}/editions`).pipe(map((response: any) => response['hydra:member']));
  }
}