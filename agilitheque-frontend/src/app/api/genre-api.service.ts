import { HttpClient } from "@angular/common/http";
import { Observable, map } from "rxjs";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { EmplacementDto } from "../model/emplacement.dto";
import { GenreDto } from "../model/genre.dto";

@Injectable({
  providedIn: 'root'
})
export class GenreApiService {
  constructor(private http: HttpClient) {}

  get(): Observable<GenreDto[]> {
    return this.http.get<GenreDto[]>(`${environment.API_URL}/genres`).pipe(map((response: any) => response['hydra:member']));
  }
}