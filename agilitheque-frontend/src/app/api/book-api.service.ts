import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Book, BookDto, CreateBookDto } from '../model/book.dto';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookApiService {
  constructor(private http: HttpClient) {}

  get(): Observable<Book[]> {
    return this.http.get<BookDto[]>(`${environment.API_URL}/livre`).pipe(map(books => books.map((book: BookDto) => new Book(book))));
  }

  create(book: CreateBookDto): Observable<Book> {
    return this.http.post<BookDto>(`${environment.API_URL}/livre`, book).pipe(map(book => new Book(book)));
  }

  update(book: CreateBookDto): Observable<any> {
    return this.http.put(`${environment.API_URL}/livre/${book.id}`, book);
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${environment.API_URL}/livre/${id}`);
  }
}
