import {Component, Input} from '@angular/core';
import {UserLoginComponent} from "../../components/user-login/user-login.component";
import {UserInscriptionComponent} from "../../components/user-inscription/user-inscription.component";
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { UserListComponent } from '../../components/user-list/user-list.component';
import { EditUserComponent } from '../../components/edit-user/edit-user.component';
import { CreateUtilisateurDto, Utilisateur } from '../../model/utilisateur.dto';
import { UtilisateurApiService } from '../../api/utilisateur-api.service';
import { ToastService } from '../../services/toaster.service';

@Component({
  selector: 'app-utilisateur-view',
  standalone: true,
  imports: [
    UserLoginComponent,
    UserInscriptionComponent,
    EditUserComponent,
    MatButtonModule,
    MatIconModule,
    UserListComponent
  ],
  templateUrl: './utilisateur-view.component.html',
  styleUrl: './utilisateur-view.component.scss'
})
export class UtilisateurViewComponent {
  users: Utilisateur[] = [];
  selectedUser: Utilisateur | undefined = undefined;

  constructor(private utilisateurApiService: UtilisateurApiService, private toastService: ToastService) {}

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers(selectionId?: number): void {
    this.utilisateurApiService.get().subscribe(users => {
      this.users = users;
      /*if(selectionId) {
        this.selectedAuthor = this.authors.find(author => author.id === selectionId);
      } else {
        this.selectedAuthor = this.selectedAuthor ? this.authors.find(author => author.id === this.selectedAuthor?.id) : undefined;
      }*/
    });
  }


  selectUser(user: Utilisateur): void {
    if(this.selectedUser && !this.selectedUser.id) {
      this.users = this.users.filter(a => a !== this.selectedUser);
    }
    this.selectedUser = user;
  }

  addUser(): void {
    const userToAdd: Utilisateur = {prenom: '', nom: '', login: '', password: '', flag_niv: 'user'};
    this.users.push(userToAdd);
    this.selectedUser = userToAdd;
  }

  saveUser(user: CreateUtilisateurDto): void {
    if(user.id) {
      this.utilisateurApiService.update(user).subscribe(() => {
        this.toastService.addToast({message: 'Utilisateur modifié avec succès', duration: 3000})
        this.loadUsers();
      });
    }
    else {
      this.utilisateurApiService.create(user).subscribe(() => {
        this.toastService.addToast({message: 'Utilisateur créé avec succès', duration: 3000})
        this.loadUsers();
      });
    }
  }

  deleteUser(): void {
    if(this.selectedUser) {
      this.utilisateurApiService.delete(this.selectedUser.id!).subscribe(() => {
        this.toastService.addToast({message: 'Utilisateur supprimé avec succès', duration: 3000})
        this.loadUsers();
      });
    }
  }
}
