import { Component, ElementRef, ViewChild } from '@angular/core';
import { BookListComponent } from '../../components/book-list/book-list.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { Book } from '../../model/book.dto';
import { BookApiService } from '../../api/book-api.service';
import { ToastService } from '../../services/toaster.service';
import { ReservationListComponent } from '../../components/reservation-list/reservation-list.component';
import { ReservationApiService } from '../../api/reservation-api.service';
import { CreateReservationDto, ReservationDto } from '../../model/reservation.dto';
import { UserService } from '../../services/user.service';
import { Utilisateur } from '../../model/utilisateur.dto';

@Component({
  selector: 'app-reservation-view',
  standalone: true,
  imports: [BookListComponent, MatButtonModule, MatIconModule, ReservationListComponent],
  templateUrl: './reservation-view.component.html',
  styleUrl: './reservation-view.component.scss'
})
export class ReservationViewComponent {
  @ViewChild('list') bookList: ElementRef<HTMLDivElement> | undefined;

  books: Book[] = [];
  selectedBook: Book | undefined = undefined;

  constructor(private bookApiService: BookApiService, private toastService: ToastService, 
    private reservationApiService: ReservationApiService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.loadBooks();
  }

  loadBooks(selectionId?: number): void {
    this.bookApiService.get().subscribe(books => {
      this.books = books;
      if(selectionId) {
        this.selectedBook = this.books.find(book => book.id === selectionId);
      } else {
        this.selectedBook = this.selectedBook ? this.books.find(book => book.id === this.selectedBook?.id) : undefined;
      }
    });
  }

  selectBook(book: Book): void {
    if(this.selectedBook && !this.selectedBook.id) {
      this.books = this.books.filter(a => a !== this.selectedBook);
    }
    this.selectedBook = book;
  }

  addReservation(): void {
    if(!this.selectedBook || !this.userService.user) {
      return;
    }

    const loggedUser: Utilisateur = this.userService.user;

    let dateEffective = new Date();
    if(this.selectedBook.reservations) {
      let minDate = new Date();
      for(const reservation of this.selectedBook.reservations) {
        if(reservation.dateEffective > minDate) {
          minDate = reservation.dateEffective;
        }
      }
      dateEffective = minDate;
      dateEffective.setDate(minDate.getDate() + 7);
    }

    const newReservation: CreateReservationDto = {idUtilisateur: 1, dateEffective, idLivre: this.selectedBook.id!};
    this.reservationApiService.create(newReservation).subscribe(reservation => {
      if(this.selectedBook) {
        if(!this.selectedBook.reservations) {
          this.selectedBook.reservations = [];
        }
        reservation.utilisateur = { id: loggedUser.id!, ...loggedUser};
        this.selectedBook.reservations.push(reservation);
        this.toastService.addToast({message: 'Reservation ajoutée !', duration: 3000})
      }
    });
  }
}
