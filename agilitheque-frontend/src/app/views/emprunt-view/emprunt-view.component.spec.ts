import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpruntViewComponent } from './emprunt-view.component';

describe('EmpruntViewComponent', () => {
  let component: EmpruntViewComponent;
  let fixture: ComponentFixture<EmpruntViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EmpruntViewComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EmpruntViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
