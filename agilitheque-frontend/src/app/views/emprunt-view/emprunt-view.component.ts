import { Component, ElementRef, ViewChild } from '@angular/core';
import { Emprunt } from '../../model/emprunt.dto';
import { EmpruntApiService } from '../../api/emprunt-api.service';
import { ToastService } from '../../services/toaster.service';
import { EmpruntListComponent } from '../../components/emprunt-list/emprunt-list.component';
import { ReservationListComponent } from '../../components/reservation-list/reservation-list.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectChange, MatSelectModule } from '@angular/material/select';
import { Book } from '../../model/book.dto';
import { BookApiService } from '../../api/book-api.service';
import { MatInputModule } from '@angular/material/input';
import { DatePipe, UpperCasePipe } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_DATE_LOCALE, provideNativeDateAdapter } from '@angular/material/core';
import { Reservation } from '../../model/reservation.dto';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { UtilisateurApiService } from '../../api/utilisateur-api.service';
import { Utilisateur } from '../../model/utilisateur.dto';
import { ReservationApiService } from '../../api/reservation-api.service';

@Component({
  selector: 'app-emprunt-view',
  standalone: true,
  imports: [EmpruntListComponent, ReservationListComponent, MatButtonModule, MatIconModule, FormsModule, 
    MatFormFieldModule, MatSelectModule, MatInputModule, DatePipe, MatDatepickerModule, MatSlideToggleModule, UpperCasePipe],
  providers: [{provide: MAT_DATE_LOCALE, useValue: 'fr-FR'}, provideNativeDateAdapter()],
  templateUrl: './emprunt-view.component.html',
  styleUrl: './emprunt-view.component.scss'
})
export class EmpruntViewComponent {
  @ViewChild('list') empruntList: ElementRef<HTMLDivElement> | undefined;

  emprunts: Emprunt[] = [];
  selectedEmprunt: Emprunt | undefined = undefined;

  books: Book[] = [];
  booksCreation: Book[] = [];
  selectedBook: Book | undefined = undefined;

  selectedReservation?: Reservation = undefined;

  creating = false;
  history = false;

  onReservation = false;

  utilisateurs: Utilisateur[] = [];
  selectedUtilisateur?: Utilisateur = undefined;

  constructor(private empruntApiService: EmpruntApiService, 
    private toastService: ToastService, 
    private bookApiService: BookApiService,
    private utilisateurApiService: UtilisateurApiService,
    private reservationApiService: ReservationApiService
  ) {}

  ngOnInit(): void {
    this.loadEmprunts();
  }

  loadEmprunts(selectionId?: number): void {
    this.empruntApiService.get().subscribe(emprunts => {
      this.emprunts = emprunts;
      let empruntToSelect = undefined;
      if(selectionId) {
        empruntToSelect = this.emprunts.find(emprunt => emprunt.id === selectionId);
      } else {
        empruntToSelect = this.selectedEmprunt ? this.emprunts.find(emprunt => emprunt.id === this.selectedEmprunt?.id) : undefined;
      }
      if(empruntToSelect) {
        this.selectEmprunt(empruntToSelect);
      }

      this.bookApiService.get().subscribe(books => {
        this.books = books;
        this.booksCreation = books.filter(book => !emprunts.find(emprunt => !emprunt.dateRetourEffective && emprunt.livre?.id == book.id))
      });
      this.utilisateurApiService.get().subscribe(utilisateurs => this.utilisateurs = utilisateurs);
    });
  }

  selectEmprunt(emprunt: Emprunt): void {
    this.creating = false;
    if(emprunt.dateRetourEffective) {
      this.history = true;
    }
    else {
      this.history = false;
    }
    if(this.selectedEmprunt && !this.selectedEmprunt.id) {
      this.emprunts = this.emprunts.filter(a => a !== this.selectedEmprunt);
    }
    this.selectedEmprunt = emprunt;
    this.selectedBook = this.books.find(book => book.id === emprunt.livre?.id);
    this.selectedUtilisateur = emprunt.utilisateur;
  }

  addEmprunt(): void {
    this.creating = true;
    const dateFin = new Date()
    dateFin.setDate(dateFin.getDate() + 7);
    const newEmprunt: Emprunt = {dateInit: new Date(), dateFin: dateFin};
    this.emprunts.push(newEmprunt);
    this.selectedEmprunt = newEmprunt;
    this.selectedBook = undefined;
    setTimeout(() => {
      if(this.empruntList) {
        this.empruntList.nativeElement.scrollTo(0, this.empruntList.nativeElement.scrollHeight);
      }
    });
  }

  selectBook(event: MatSelectChange): void {
    this.selectedBook = this.books.find(book => book.id === event.value);
    if(this.selectedBook && this.selectedBook.reservations && this.selectedBook.reservations.length > 0) {
      this.selectedReservation = this.selectedBook.reservations[0];
    }
  }

  selectUtilisateur(event: MatSelectChange): void {
    this.selectedUtilisateur = this.utilisateurs.find(utilisateur => utilisateur.id === event.value);
  }

  saveEmprunt(): void {
    if(this.creating) {
      if(this.selectedEmprunt && this.selectedBook && this.selectedEmprunt.dateFin) {
        let utilisateurId = undefined;
        if(this.onReservation && this.selectedReservation) {
          utilisateurId = this.selectedReservation.utilisateur.id;
        }
        else if(!this.onReservation && this.selectedUtilisateur) {
          utilisateurId = this.selectedUtilisateur.id;
        }
        else {
          return;
        }
        this.empruntApiService.create(
          {
            idUtilisateur: utilisateurId!,
            idLivre: this.selectedBook.id!,
            dateInit: this.selectedEmprunt.dateInit!,
            dateFin: this.selectedEmprunt.dateFin
          }
        ).subscribe((emprunt) => {
          if(this.onReservation && this.selectedReservation) {
            this.reservationApiService.delete(this.selectedReservation.id!).subscribe(() => {
              this.bookApiService.get().subscribe(books => this.books = books);
            });
          }
          this.toastService.addToast({message: 'Emprunt créé avec succès', duration: 3000});
          this.loadEmprunts(emprunt.id);
        });
      }
    }
    else {
      if(this.selectedEmprunt && this.selectedEmprunt.id && this.selectedEmprunt.dateRetourEffective) {
        this.empruntApiService.update({
          id: this.selectedEmprunt.id,
          idUtilisateur: this.selectedEmprunt.utilisateur!.id,
          idLivre: this.selectedBook!.id!,
          dateInit: this.selectedEmprunt.dateInit!,
          dateFin: this.selectedEmprunt.dateFin!,
          dateRetourEffective: this.selectedEmprunt.dateRetourEffective
        }).subscribe(() => {
          this.toastService.addToast({message: 'Emprunt mis à jour avec succès', duration: 3000});
          this.loadEmprunts(this.selectedEmprunt?.id);
        });
      }
    }
  }
}
