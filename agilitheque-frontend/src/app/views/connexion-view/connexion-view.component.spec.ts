import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnexionViewComponent } from './connexion-view.component';

describe('ConnexionViewComponent', () => {
  let component: ConnexionViewComponent;
  let fixture: ComponentFixture<ConnexionViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ConnexionViewComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ConnexionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
