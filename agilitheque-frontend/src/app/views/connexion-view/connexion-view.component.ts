import { Component, OnInit } from '@angular/core';
import {UserLoginComponent} from "../../components/user-login/user-login.component";

@Component({
  selector: 'app-connexion-view',
  standalone: true,
  templateUrl: './connexion-view.component.html',
  imports: [
    UserLoginComponent
  ],
  styleUrls: ['./connexion-view.component.scss']
})
export class ConnexionViewComponent {
}
