import { Component } from '@angular/core';
import {UserInscriptionComponent} from "../../components/user-inscription/user-inscription.component";

@Component({
  selector: 'app-inscription-view',
  standalone: true,
  imports: [
    UserInscriptionComponent
  ],
  templateUrl: './inscription-view.component.html',
  styleUrl: './inscription-view.component.scss'
})
export class InscriptionViewComponent {

}
