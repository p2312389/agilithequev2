import { Routes } from '@angular/router';
import { HomeViewComponent } from './views/home-view/home-view.component';
import { AuthorViewComponent } from './views/author-view/author-view.component';
import { BookViewComponent } from './views/book-view/book-view.component';
import { ConnexionViewComponent } from './views/connexion-view/connexion-view.component';
import { EmpruntViewComponent } from './views/emprunt-view/emprunt-view.component';
import { ReservationViewComponent } from './views/reservation-view/reservation-view.component';
import { UtilisateurViewComponent } from './views/utilisateur-view/utilisateur-view.component';
import {InscriptionViewComponent} from "./views/inscription-view/inscription-view.component";

export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeViewComponent },
  { path: 'authors', component: AuthorViewComponent },
  { path: 'books', component: BookViewComponent },
  { path: 'connexion', component: ConnexionViewComponent },
  { path: 'emprunts', component: EmpruntViewComponent },
  { path: 'reservations', component: ReservationViewComponent },
  { path: 'utilisateurs', component: UtilisateurViewComponent },
  {path: 'inscription', component: InscriptionViewComponent}
];
