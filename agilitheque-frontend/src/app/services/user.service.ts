import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UtilisateurApiService } from '../api/utilisateur-api.service';
import { Utilisateur } from '../model/utilisateur.dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userSubject: BehaviorSubject<Utilisateur | null> = new BehaviorSubject<Utilisateur | null>(null);
  public userObservable: Observable<Utilisateur | null> = this.userSubject.asObservable();
  public user: Utilisateur | null = null;

  constructor(private readonly utilisateurApiService: UtilisateurApiService) {
    if(localStorage.getItem('user')) {
      this.setUser(JSON.parse(localStorage.getItem('user') as string));
    }
  }

  setUser(user: Utilisateur | null | undefined): void {
    if (user !== undefined) {
      this.user = user;
      this.userSubject.next(user);
      localStorage.setItem('user', JSON.stringify(user));
    }
  }

  login(login: string, password: string): Observable<Utilisateur | null>{
    return new Observable<Utilisateur | null>(observer => {
      this.utilisateurApiService.login(login, password).subscribe({
        next: (user) => {
          this.setUser(user);
          observer.next(user);
          observer.complete();
        }, error: (error) => {
          this.setUser(null);
          observer.error(error);
          observer.complete();
        }});
    });
  }

  logout(): void {
    this.setUser(null);
    localStorage.removeItem('user');
  }
}
