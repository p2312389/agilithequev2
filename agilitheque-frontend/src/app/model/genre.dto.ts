export interface GenreDto {
  id: number;
  libelle: string;
}