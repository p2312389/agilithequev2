export interface EditionDto {
  id: number;
  nomEdition: string;
}