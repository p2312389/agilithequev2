import { Book, BookDto } from "./book.dto";
import { UtilisateurDto } from "./utilisateur.dto";

export interface EmpruntDto {
  id: number;
  utilisateur: UtilisateurDto;
  livre: BookDto;
  dateInit: string;
  dateFin: string;
  dateRetourEffective?: string;
}

export class Emprunt {
  id?: number;
  utilisateur?: UtilisateurDto;
  livre?: Book;
  dateInit?: Date;
  dateFin?: Date;
  dateRetourEffective?: Date;

  constructor(dto: EmpruntDto) {
    this.id = dto.id;
    this.utilisateur = dto.utilisateur;
    this.livre = new Book(dto.livre);
    this.dateInit = new Date(dto.dateInit);
    this.dateFin = new Date(dto.dateFin);
    if (dto.dateRetourEffective) {
      this.dateRetourEffective = new Date(dto.dateRetourEffective);
    }
  }
}

export interface CreateEmpruntDto {
  id?: number;
  idUtilisateur: number;
  idLivre: number;
  dateInit: Date;
  dateFin: Date;
  dateRetourEffective?: Date;
}