export interface AuthorDto {
  id: number;
  prenom: string;
  nom: string;
  imageSrc?: string;
}

export class Author {
  id?: number;
  prenom: string;
  nom: string;
  imageSrc?: string;

  constructor(dto: AuthorDto) {
    this.id = dto.id;
    this.prenom = dto.prenom;
    this.nom = dto.nom;
    this.imageSrc = dto.imageSrc;
  }
}

export interface CreateAuthorDto {
  id?: number;
  prenom: string;
  nom: string;
  imageSrc?: string;
}