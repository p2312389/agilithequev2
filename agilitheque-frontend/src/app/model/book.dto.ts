import { AuthorDto } from "./author.dto";
import { EditionDto } from "./edition.dto";
import { EmplacementDto } from "./emplacement.dto";
import { GenreDto } from "./genre.dto";
import { Reservation, ReservationDto } from "./reservation.dto";

export interface BookDto {
  id: number;
  libelle: string;
  resume?: string;
  imageSrc?: string;
  auteur: AuthorDto;
  annee: number;
  emplacement: EmplacementDto;
  genre: GenreDto;
  edition: EditionDto;
  reservations?: ReservationDto[];
}

export class Book {
  id?: number;
  libelle: string;
  resume?: string;
  imageSrc?: string;
  auteur?: AuthorDto;
  annee: number;
  emplacement?: EmplacementDto;
  genre?: GenreDto;
  edition?: EditionDto;
  reservations?: Reservation[];

  constructor(dto: BookDto) {
    this.id = dto.id;
    this.libelle = dto.libelle;
    this.resume = dto.resume;
    this.imageSrc = dto.imageSrc;
    this.auteur = dto.auteur;
    this.annee = dto.annee;
    this.emplacement = dto.emplacement;
    this.genre = dto.genre;
    this.edition = dto.edition;
    this.reservations = dto.reservations?.map(reservation => new Reservation(reservation));
  }
}

export interface CreateBookDto {
  id?: number;
  libelle: string;
  resume?: string;
  imageSrc?: string;
  idAuteur: number;
  annee: number;
  idEmplacement: number;
  idGenre: number;
  idEdition: number;
}