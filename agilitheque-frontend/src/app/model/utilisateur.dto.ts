export interface UtilisateurDto {
  id: number;
  prenom: string;
  nom: string;
  flag_niv: 'admin' | 'user';
  login: string;
  password?: string;
}

export class Utilisateur {
  id?: number;
  prenom: string;
  nom: string;
  flag_niv: 'admin' | 'user';
  login: string;
  password?: string;

  constructor(dto: UtilisateurDto) {
    this.id = dto.id;
    this.prenom = dto.prenom;
    this.nom = dto.nom;
    this.flag_niv = dto.flag_niv;
    this.login = dto.login;
    this.password = dto.password;
  }
}

export interface CreateUtilisateurDto {
  id?: number;
  prenom: string;
  nom: string;
  flag_niv: 'admin' | 'user';
  login: string;
  password: string;
}