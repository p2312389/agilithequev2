import { UtilisateurDto } from "./utilisateur.dto";

export interface ReservationDto {
  id: number;
  utilisateur: UtilisateurDto;
  dateEffective: string;
}

export class Reservation {
  id?: number;
  utilisateur: UtilisateurDto;
  dateEffective: Date;

  constructor(dto: ReservationDto) {
    this.id = dto.id;
    this.utilisateur = dto.utilisateur;
    this.dateEffective = new Date(dto.dateEffective);
  }
}

export interface CreateReservationDto {
  idUtilisateur: number;
  dateEffective: Date;
  idLivre: number;
}