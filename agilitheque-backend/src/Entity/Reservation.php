<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ReservationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
#[ApiResource(normalizationContext: ['groups' => ['reservation']])]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['reservation', 'livre', 'emprunt'])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    #[Groups(['reservation', 'emprunt'])]
    private ?Livre $livre = null;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    #[Groups(['reservation', 'livre', 'emprunt'])]
    private ?Utilisateur $utilisateur = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(['reservation', 'livre', 'emprunt'])]
    private ?\DateTimeInterface $dateEffective = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLivre(): ?Livre
    {
        return $this->livre;
    }

    public function setLivre(?Livre $livre): static
    {
        $this->livre = $livre;

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): static
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getDateEffective(): ?\DateTimeInterface
    {
        return $this->dateEffective;
    }

    public function setDateEffective(?\DateTimeInterface $dateEffective): static
    {
        $this->dateEffective = $dateEffective;

        return $this;
    }
}
