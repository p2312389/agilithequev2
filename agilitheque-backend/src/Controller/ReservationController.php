<?php

namespace App\Controller;

use App\Entity\Livre;
use App\Entity\Reservation;
use App\Repository\LivreRepository;
use App\Repository\ReservationRepository;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('api')]
class ReservationController extends AbstractController
{
    private SerializerInterface $serializer;
    private LivreRepository $livreRepository;
    private UtilisateurRepository $utilisateurRepository;
    private ReservationRepository $reservationRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        SerializerInterface    $serializer,
        LivreRepository        $livreRepository,
        UtilisateurRepository  $utilisateurRepository,
        ReservationRepository  $reservationRepository,
        EntityManagerInterface $entityManager)
    {
        $this->serializer = $serializer;
        $this->livreRepository = $livreRepository;
        $this->utilisateurRepository = $utilisateurRepository;
        $this->reservationRepository = $reservationRepository;
        $this->entityManager = $entityManager;
    }

    #[Route('/reservation', name: 'recuperer-reservations', methods: 'GET')]
    public function recupererReservations()
    {
        $reservations = $this->reservationRepository->findAll();
        return new JsonResponse($this->serializer->serialize($reservations, 'json', ['groups' => 'reservation']), Response::HTTP_OK, [], true);
    }

    #[Route('/reservation/{id}', name: 'recuperer-reservation', methods: 'GET')]
    public function recupererReservation(Reservation $reservation)
    {
        return new JsonResponse($this->serializer->serialize($reservation, 'json', ['groups' => 'reservation']), Response::HTTP_OK, [], true);
    }

    #[Route('/reservation/{id}', name: 'supprimer-reservation', methods: 'DELETE')]
    public function supprimerReservation(Reservation $reservation)
    {
        $this->entityManager->remove($reservation);
        $this->entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/reservation', name: 'creer-reservation', methods: 'POST')]
    public function creerReservation(Request $request)
    {
        $reservation = $this->serializer->deserialize($request->getContent(), Reservation::class, 'json');

        // Récupération de l'ensemble des données envoyées sous forme de tableau
        $content = $request->toArray();

        if (!array_key_exists('idLivre', $content) || !array_key_exists('idUtilisateur', $content)) {

            return new JsonResponse("Veuillez renseigner tous les champs nécessaires", Response::HTTP_NOT_FOUND, [], true);
        }

        $idLivre = $content['idLivre'];
        $idUtilisateur = $content['idUtilisateur'];

        $reservation->setLivre($this->livreRepository->find($idLivre));
        $reservation->setUtilisateur($this->utilisateurRepository->find($idUtilisateur));

        $this->entityManager->persist($reservation);
        $this->entityManager->flush();

        $jsonReservation = $this->serializer->serialize($reservation, 'json', ['groups' => 'reservation']);

        return new JsonResponse($jsonReservation, Response::HTTP_CREATED, [], true);
    }
}
