<?php

namespace App\Controller;

use App\Entity\Emprunt;
use App\Repository\EmpruntRepository;
use App\Repository\LivreRepository;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('api')]
class EmpruntController extends AbstractController
{
    private SerializerInterface $serializer;
    private LivreRepository $livreRepository;
    private UtilisateurRepository $utilisateurRepository;
    private EmpruntRepository $empruntRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        SerializerInterface $serializer,
        LivreRepository $livreRepository,
        UtilisateurRepository $utilisateurRepository,
        EmpruntRepository $empruntRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->serializer = $serializer;
        $this->livreRepository = $livreRepository;
        $this->utilisateurRepository = $utilisateurRepository;
        $this->empruntRepository = $empruntRepository;
        $this->entityManager = $entityManager;
    }

    #[Route('/emprunt', name: 'recuperer-emprunts', methods: 'GET')]
    public function recupererEmprunts()
    {
        $reservations = $this->empruntRepository->findAll();
        return new JsonResponse($this->serializer->serialize($reservations, 'json', ['groups' => 'emprunt']), Response::HTTP_OK, [], true);
    }

    #[Route('/emprunt/{id}', name: 'recuperer-emprunt', methods: 'GET')]
    public function recupererEmprunt(Emprunt $emprunt)
    {
        return new JsonResponse($this->serializer->serialize($emprunt, 'json', ['groups' => 'emprunt']), Response::HTTP_OK, [], true);
    }

    #[Route('/emprunt/{id}', name: 'supprimer-emprunt', methods: 'DELETE')]
    public function supprimerEmprunt(Emprunt $emprunt)
    {
        $this->entityManager->remove($emprunt);
        $this->entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/emprunt', name: 'creer-emprunt', methods: 'POST')]
    public function creerEmprunt(Request $request)
    {
        $emprunt = $this->serializer->deserialize($request->getContent(), Emprunt::class, 'json');

        // Récupération de l'ensemble des données envoyées sous forme de tableau
        $content = $request->toArray();

        if (!array_key_exists('idLivre', $content) || !array_key_exists('idUtilisateur', $content)) {

            return new JsonResponse("Veuillez renseigner tous les champs nécessaires", Response::HTTP_NOT_FOUND, [], true);
        }

        $idLivre = $content['idLivre'];
        $idUtilisateur = $content['idUtilisateur'];

        $emprunt->setLivre($this->livreRepository->find($idLivre));
        $emprunt->setUtilisateur($this->utilisateurRepository->find($idUtilisateur));

        $this->entityManager->persist($emprunt);
        $this->entityManager->flush();

        $jsonEmprunt = $this->serializer->serialize($emprunt, 'json', ['groups' => 'emprunt']);

        return new JsonResponse($jsonEmprunt, Response::HTTP_CREATED, [], true);
    }

    #[Route('/emprunt/{id}', name: 'modifier-emprunt', methods: 'PUT')]
    public function modifierEmprunt(Request $request, Emprunt $emprunt)
    {
        $emprunt = $this->serializer->deserialize(
            $request->getContent(),
            Emprunt::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $emprunt]
        );

        // Récupération de l'ensemble des données envoyées sous forme de tableau
        $content = $request->toArray();

        if (!array_key_exists('idLivre', $content) || !array_key_exists('idUtilisateur', $content)) {

            return new JsonResponse("Veuillez renseigner tous les champs nécessaires", Response::HTTP_NOT_FOUND, [], true);
        }

        $idLivre = $content['idLivre'];
        $idUtilisateur = $content['idUtilisateur'];

        $emprunt->setLivre($this->livreRepository->find($idLivre));
        $emprunt->setUtilisateur($this->utilisateurRepository->find($idUtilisateur));

        $this->entityManager->persist($emprunt);
        $this->entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
