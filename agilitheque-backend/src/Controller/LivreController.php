<?php

namespace App\Controller;

use App\Entity\Livre;
use App\Repository\AuteurRepository;
use App\Repository\EditionRepository;
use App\Repository\EmplacementRepository;
use App\Repository\GenreRepository;
use App\Repository\LivreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('api')]
class LivreController extends AbstractController
{
    private SerializerInterface $serializer;
    private AuteurRepository $auteurRepository;
    private EmplacementRepository $emplacementRepository;
    private LivreRepository $livreRepository;
    private GenreRepository $genreRepository;
    private EditionRepository $editionRepository;
    private EntityManagerInterface $entityManager;
    public function __construct(
        SerializerInterface $serializer,
        AuteurRepository $auteurRepository,
        EntityManagerInterface $entityManager,
        EmplacementRepository $emplacementRepository,
        EditionRepository $editionRepository,
        GenreRepository $genreRepository,
        LivreRepository $livreRepository
    )
    {
        $this->serializer = $serializer;
        $this->auteurRepository = $auteurRepository;
        $this->emplacementRepository = $emplacementRepository;
        $this->editionRepository = $editionRepository;
        $this->genreRepository = $genreRepository;
        $this->entityManager = $entityManager;
        $this->livreRepository = $livreRepository;
    }

    #[Route('/livre', name: 'recuperer-livres', methods: 'GET')]
    public function recupererLivres()
    {
        $livres = $this->livreRepository->findAll();
        return new JsonResponse($this->serializer->serialize($livres, 'json', ['groups' => 'livre']), Response::HTTP_OK, [], true);
    }

    #[Route('/livre/{id}', name: 'recuperer-livre', methods: 'GET')]
    public function recupererLivre(Livre $livre)
    {
        return new JsonResponse($this->serializer->serialize($livre, 'json', ['groups' => 'livre']), Response::HTTP_OK, [], true);
    }

    #[Route('/livre/{id}', name: 'supprimer-livres', methods: 'DELETE')]
    public function supprimerLivre(Livre $livre)
    {
        $this->entityManager->remove($livre);
        $this->entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    #[Route('/livre/{id}', name: 'modifier-livre', methods: 'PUT')]
    public function modifierLivre(Request $request, Livre $livre)
    {
        $updatedBook = $this->serializer->deserialize($request->getContent(),
            Livre::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $livre]);
        $content = $request->toArray();
        if (array_key_exists('idAuteur', $content)) {
            $updatedBook->setAuteur($this->auteurRepository->find($content['idAuteur']));
        }
        if (array_key_exists('idGenre', $content)) {
            $updatedBook->setGenre($this->genreRepository->find($content['idGenre']));
        }
        if (array_key_exists('idEmplacement', $content)) {
            $updatedBook->setEmplacement($this->emplacementRepository->find($content['idEmplacement']));
        }
        if (array_key_exists('idEdition', $content)) {
            $updatedBook->setEdition($this->editionRepository->find($content['idEdition']));
        }
        $this->entityManager->persist($updatedBook);
        $this->entityManager->flush();
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/livre', name: 'creer-livre', methods: 'POST')]
    public function creerLivre(Request $request)
    {
        $book = $this->serializer->deserialize($request->getContent(), Livre::class, 'json');

        // Récupération de l'ensemble des données envoyées sous forme de tableau
        $content = $request->toArray();

        if (!array_key_exists('idAuteur', $content) || !array_key_exists('idGenre', $content) ||
            !array_key_exists('idEmplacement', $content) || !array_key_exists('idEdition', $content)) {

            return new JsonResponse("Veuillez renseigner tous les champs nécessaires", Response::HTTP_NOT_FOUND, [], true);
        }

        // Récupération de l'idAuthor. S'il n'est pas défini, alors on met -1 par défaut.
        $idAuteur = $content['idAuteur'];
        $idGenre = $content['idGenre'];
        $idEdition = $content['idEdition'];
        $idEmplacement = $content['idEmplacement'];

        $book->setAuteur($this->auteurRepository->find($idAuteur));
        $book->setGenre($this->genreRepository->find($idGenre));
        $book->setEmplacement($this->emplacementRepository->find($idEmplacement));
        $book->setEdition($this->editionRepository->find($idEdition));

        $this->entityManager->persist($book);
        $this->entityManager->flush();

        $jsonBook = $this->serializer->serialize($book, 'json', ['groups' => 'livre']);

        return new JsonResponse($jsonBook, Response::HTTP_CREATED, [], true);
    }
}
