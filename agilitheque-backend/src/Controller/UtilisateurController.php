<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('api')]
class UtilisateurController extends AbstractController
{
    private SerializerInterface $serializer;
    private UtilisateurRepository $utilisateurRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(UtilisateurRepository $utilisateurRepository, SerializerInterface $serializer, EntityManagerInterface $entityManager)
    {
        $this->utilisateurRepository = $utilisateurRepository;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
    }

    #[Route('/utilisateur/connexion', name: 'connexion-utilisateur', methods: 'POST')]
    public function connexion(Request $request)
    {
        $content = $request->toArray();

        if (!array_key_exists('login', $content) || !array_key_exists('password', $content)) {
            return new JsonResponse("Veuillez renseigner un login et un mot de passe", Response::HTTP_BAD_REQUEST, [], true);
        }

        $login = $content['login'];
        $password = $content['password'];

        $utilisateur = $this->utilisateurRepository->rechercherLoginMotDePasse($login, $password);

        if (count($utilisateur) === 0) {
            return new JsonResponse("L'utilisateur n'existe pas", Response::HTTP_INTERNAL_SERVER_ERROR, [], true);
        }

        return new JsonResponse($this->serializer->serialize($utilisateur[0], 'json'), Response::HTTP_OK, [], true);
    }

    #[Route('/utilisateur', name: 'recuperer-utilisateurs', methods: 'GET')]
    public function recupererUtilisateurs(Request $request)
    {
        $utilisateurs = $this->utilisateurRepository->findAll();
        return new JsonResponse($this->serializer->serialize($utilisateurs, 'json', ['groups' => 'utilisateur']), Response::HTTP_OK, [], true);

    }

    #[Route('/utilisateur/{id}', name: 'supprimer-utilisateur', methods: 'DELETE')]
    public function supprimerUtilisateur(Utilisateur $utilisateur)
    {
        $this->entityManager->remove($utilisateur);
        $this->entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/utilisateur', name: 'creer-utilisateur', methods: 'POST')]
    public function creerUtilisateur(Request $request)
    {
        $utilisateur = $this->serializer->deserialize($request->getContent(), Utilisateur::class, 'json');


        $this->entityManager->persist($utilisateur);
        $this->entityManager->flush();

        $jsonReservation = $this->serializer->serialize($utilisateur, 'json', ['groups' => 'utilisateur']);

        return new JsonResponse($jsonReservation, Response::HTTP_CREATED, [], true);
    }

    #[Route('/utilisateur/{id}', name: 'modifier-utilisateur', methods: 'PUT')]
    public function modifierUtilisateur(Request $request, Utilisateur $utilisateur)
    {
        $utilisateur = $this->serializer->deserialize($request->getContent(), Utilisateur::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $utilisateur]);

        $this->entityManager->persist($utilisateur);
        $this->entityManager->flush();

        $jsonReservation = $this->serializer->serialize($utilisateur, 'json', ['groups' => 'utilisateur']);

        return new JsonResponse($jsonReservation, Response::HTTP_CREATED, [], true);
    }
}
